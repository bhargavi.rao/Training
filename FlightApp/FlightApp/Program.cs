﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;

namespace FlightApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> myValues = new List<string>();
            string line;
            StreamReader file = new StreamReader(@"C:\Training\Provider1.txt");
            if ((line = file.ReadLine()) != null)
            {
                string[] fields = line.Split(',');
                using (SqlConnection con = new SqlConnection(@"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = Flights; Integrated Security = True; Pooling = False"))
                {

                    con.Open();
                    while ((line = file.ReadLine()) != null)
                    {
                        SqlCommand cmd = new SqlCommand("INSERT INTO Provider1(Origin,Departure Time,Destination, Destination Time,Price,Id) VALUES (@origin,@departuretime,@destination,@destinationtime,@price,@id)", con);
                        cmd.Parameters.AddWithValue("@origin", fields[0].ToString());
                        cmd.Parameters.AddWithValue("@departuretime", fields[1].ToString());
                        cmd.Parameters.AddWithValue("@destination", fields[2].ToString());
                        cmd.Parameters.AddWithValue("@destinationtime", fields[3].ToString());
                        cmd.Parameters.AddWithValue("@price", fields[4].ToString());
                        cmd.Parameters.AddWithValue("@id", fields[5].ToString());
                        cmd.ExecuteNonQuery();
                    }
                }


            }
        }
    }
}
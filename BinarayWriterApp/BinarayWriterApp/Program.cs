﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinarayWriterApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ////Open or create a file
            //FileStream fs =File.OpenWrite(@"c:\Training\products.txt");
            //BinaryWriter writer = new BinaryWriter(fs);
            //writer.Write(123);

            //writer.Write("Product 1");

            ////product price as double
            //writer.Write(34.55);

            ////close writer stream
            //writer.Close();
            //fs.Close();

            //Console.WriteLine("Product details are written to a file");


            using (FileStream fs = File.OpenRead(@"c:\Training\products.txt"))
            {
                BinaryReader reader = new BinaryReader(fs);

                Console.WriteLine("Product ID : {0}", reader.ReadInt32());
                Console.WriteLine("Prouct Name: {0}", reader.ReadString());
                Console.WriteLine("Prouct Price: {0}", reader.ReadDouble());

                reader.Close();
                fs.Close();
            }
            Console.ReadLine();
        }
    }
}

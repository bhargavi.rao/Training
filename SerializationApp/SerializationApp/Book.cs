﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializationApp
{
    [Serializable]
    public class Book
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public double Price { get; set; }

        string publisher;

        public void SetPublisher(String publisher)
        {
             this.publisher = publisher;

        }

        public override string ToString()
        {
            return string.Format("ID: {0}\tName: {1}\tPrice:{2}\tPublisher: {3}",BookId,BookName,Price,this.publisher);
        }
    }
}

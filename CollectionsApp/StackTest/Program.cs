﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack stack = new Stack()
            bool aBoolean = true;
            char aChar = '$';
            int anInt = 34567;
            string aString = "Hello";

            //use push()
            stack.Push(aBoolean);
            PrintStack(stack);
            stack.Push(aChar);
            PrintStack(stack);
            stack.Push(anInt);
            PrintStack(stack);
            stack.Push(aString);
            PrintStack(stack);

            Console.WriteLine("first item in stack isn {0}\n", stack.Peek());
            PrintStack(stack);

            object o = stack.Pop();
            PrintStack(stack);




            Console.ReadLine();

        }

        private static void PrintStack<T>(Stack<T> stack)
        {
            if (stack.Count == 0)

            {
                Console.WriteLine("Stack is empty\n");

            }
            else
            {
                Console.WriteLine("Stack is:");

                foreach (var item in stack)
                {
                    Console.WriteLine("{0}", item);
                }

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections; 

namespace CollectionsApp
{
    class Program
    {
        private static int[] intValues = { 1, 2, 3, 4, 5, 6};
        private static double[] doubleValues = { 1.4, 2.4, 6.7, 3.6, 5.4, 5.8}

        private static int[] intValuesCopy;


        static void Main(string[] args)
        {
            //1.Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial array values:\n");
            PrintArrays();

            //Sort doubleValues
            Array.Sort(doubleValues);

            //copy values from intValues to intValuesCopy array
            Array.Copy(intValues, intValuesCopy, intValues.Length);

            Console.WriteLine("\nArray values after Sort and Copy:\n");
            PrintArrays();

            //search 5 in intValues array

            int result = Array.BinarySearch(intValues, 5);
            if (result >= 0)
            {
                Console.WriteLine("5 found at element {0} in intValues", result);
            }
            else
            {
                Console.WriteLine("5 not found in intValues!");
            }
            Console.ReadLine();
        }

        private static void PrintArrays()
        {
            Console.WriteLine("doubleValues: ");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");

            }
            Console.WriteLine("\nintValues: "");
            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())

            {
                Console.WriteLine(enumerator.Current + "");
            }
            Console.WriteLine("\nintValues.Copy: ");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine("element + " ");
            }
        }

        }
    }
}

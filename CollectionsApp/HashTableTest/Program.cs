﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Text.RegularExpressions;

namespace HashTableTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Hashtable hashtable = CollectWords();
            DisplayHashtable(hashtable);

            Console.ReadLine();
        }

        private static void DisplayHashtable(Hashtable hashtable)
        {
            Console.WriteLine("Has table contains: \n {0,-12},{1,-12}", "Key:", "Value");
            foreach (var key in hashtable.Keys)

            {
                Console.WriteLine("{0,-12},{1,-12}", key, hashtable[key]);
            }
            Console.WriteLine("\nSize:{0}", hashtable.Count);
        }
        
        private static Hashtable CollectWords()
        {

            Hashtable hashtable = new Hashtable();
            Console.WriteLine("Enter a string");
            string input = Console.ReadLine();
            string[] words = Regex.Split(input, @"\n+");

            foreach (var word in words)
            {
                string wordKey = word.ToLower();
                if (HashTableTest.ContainsKey(wordKey))
                {
                    HashTableTest[wordKey] = ((int)HashTableTest[wordKey]) + 1;

                }

                else
                {
                    table.Add(wordKey, 1);
                }
            }

        }

    }
}

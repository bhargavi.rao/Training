﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Flight Network");
            Console.WriteLine("Please enter the name");
            string.uName = Console.ReadLine();
            Console.WriteLine("Please enter the password");
            string pwd = Console.ReadLine();

            Login login = new Login();
            login.loginSuccess += OnSuccess;
            login.loginFailed  += OnFail;

            User user(UserName = uName, Password = pwd);


        }
        static void OnSuccess(LoginEventArgs, EventArgs)
        {
            Console.WriteLine("HEllo {0},login successful with {1});
        }
        static void OnFail(LoginEventArgs, EventArgs )
        {
            Console.WriteLine("Hello {0},login failed with {1});
        }
    }
}

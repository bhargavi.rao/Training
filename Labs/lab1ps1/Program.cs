﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1ps1
{
    class Date
    {
        public int mDay;
        public int mMonth;
        public int mYear;
        public Date()
        {
            mDay = 23;
            mMonth = 03;
            mYear = 1996;
        }
        public Date(int dd, int mm, int yyyy)
        {
            mDay = dd;
            mMonth = mm;
            mYear = yyyy;
        }
        public void PrintDate()
        {
            Console.WriteLine("Date" + mDay + "." + mMonth + "." + mYear);
        }
        public static void Main(string[] args)
        {
            Date d1 = new Date();
            Date d2 = new Date(23, 03, 1996);
            d1.PrintDate();
            d2.PrintDate();
        }
    }
}


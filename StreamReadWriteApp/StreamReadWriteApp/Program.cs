﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace StreamReadWriteApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //create file withe enable appending "true"
            StreamWriter writer = new StreamWriter(@"c:\Training\data.txt", true);

            writer.WriteLine("This is line 1");
            writer.WriteLine("This is line 2");
            writer.WriteLine("This is line 3");

            writer.Close();

            Console.WriteLine("Press a key to read it back");

            Console.ReadKey();

            StreamReader reader = new StreamReader(@"c:\Training\data.txt");
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
            reader.Close();

            Console.ReadLine();
        }
    }
}

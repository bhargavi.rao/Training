﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Authenticate
{
    class Program
    {
        static void Main(string[] args)
        {
            login l = new login();
            l.LoginSuccessful += OnSuccess;
            l.LoginFailed += OnFail;

            user u1 = new user();
            user u2 = new user();
            u1.username = "flight";
            u1.password = "flight";
            u2.username = "test";
            u2.password = "try";
            l.LoginResult(u1);
            l.LoginResult(u2);
            Console.ReadLine();

        }

        private static void OnSuccess(LoginEventArgs x)
        {
            Console.WriteLine("Successful");
            Console.WriteLine("Username:\t" + x.username);
            Console.WriteLine("password:\t" + x.password);
        }
        private static void OnFail(LoginEventArgs y)
        {
            Console.WriteLine("Authentication failed");
            Console.WriteLine("Username:\t" + y.username);
            Console.WriteLine("password:\t" + y.password);
        }
    }
    class LoginEventArgs
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}

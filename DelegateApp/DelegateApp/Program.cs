﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    
    class Program
    {
        static void Main(string[] args)
        {
            MyButton button = new MyButton();
            button.Click += delegate ()
            {
                Console.WriteLine("MyButton clicked!!!");

            };

            button.RaiseEvent();
        }

        static void 

    }
    delegate void ClickHandler();


    class MyButton
    {
        public event ClickHandler Click;
        public void RaiseEvent()
        {
            Click();

        }
    }

}

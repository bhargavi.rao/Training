﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Doorbell bell = new Doorbell();
            bell.RingBell += OpenTheDoor;
            bell.RingTheBell();
        }


        private static void OpenTheDoor(string cabinName)
        {
            Console.WriteLine("Door Opened");
            Console.WriteLine("This is {0} room!!!",cabinName);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    public class Employee
    {
        string input = "y";
        public int Emp_Id;
        public string Emp_Name;
        public double Basic_salary;
        public double HRA;
        public double gross_salary;
        public Employee(int Emp_ID,string Emp_name,double Basic_sal)
        {
            Emp_Id = Emp_ID;
            Emp_Name= Emp_name;
            Basic_sal = Basic_salary;
            double Medical = 1400;
            HRA = (Basic_sal * 8) / 100;
            gross_salary = Basic_sal + HRA + Medical;
        }
        public override string ToString()
        {
      
        return "Employee ID:" + Emp_Id + "\nEmployee name:" + Emp_Name + "\nGross salary" + gross_salary; 
        }
    }
}

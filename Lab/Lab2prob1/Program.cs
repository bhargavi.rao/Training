﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2prob1
{
    class Date
    {
        public int mDay;
        public int mMonth;
        public int mYear;
        public Date()
        {
            mDay = 1;
            mMonth = 1;
            mYear = 1987;
        }
        public Date(int dd, int mm, int yyyy)
        {
            mDay = dd;
            mMonth = mm;
            mYear = yyyy;
        }
        public void PrintDate()
        {
            Console.WriteLine("Date:" + mDay + "-" + mMonth + "-" + mYear);
        }
       
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter the day");
            int d = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the month");
            int m = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the year"); 
            int y = Convert.ToInt32(Console.ReadLine());
            
            Date d1 = new Date();
            Date d2 = new Date(d,m,y);
            d1.PrintDate();
            d2.PrintDate();
            Console.ReadLine();
        }

    }
}

//output

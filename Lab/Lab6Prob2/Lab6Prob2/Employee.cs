﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6Prob2
{
    public class Employee
    {
        public int emp_id;
        public string emp_name;
        public double basic_salary;
        public Employee(int Emp_id, string Emp_name,double Basic_salary)
        {
            emp_id = Emp_id;
            emp_name = Emp_name;
            basic_salary = Basic_salary;
        }

    }
    public class Manager : Employee
    {
        public Manager(int Emp_id, string Emp_name, double Basic_salary): base( Emp_id,Emp_name,Basic_salary)
        {
            emp_id = Emp_id;
            emp_name = Emp_name;
            basic_salary = Basic_salary;
        }
        public double Petrol_allowance= (basic_salary*8)/100;
        public double Food_allowance = (basic_salary * 13) / 100;
        public double Other_allowance = (basic_salary * 3) / 100;
     
    }
    public class President : Employee
    {
        public President(int Emp_id, string Emp_name, double Basic_salary) : base(Emp_id, Emp_name, Basic_salary)
        {
            emp_id = Emp_id;
            emp_name = Emp_name;
            basic_salary = Basic_salary;
        }
        public int KM_travelled;
        public double Tour_allowance = 8 * KM_travelled;
        public double Telephone_allowance = 2000;
    }


}

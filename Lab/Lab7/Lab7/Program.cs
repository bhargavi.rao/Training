﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle cir = new Circle();
            
            Rectangle rectangle = new Rectangle();
            string input = "y";
            while (input=="y")
            {
                Console.WriteLine("Find the area:\t1.circle\t2.rectangle");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        cir.Area();
                        break;
                    case 2:
                        rectangle.Area();
                        break;
                    default:
                        break;

                }
                Console.WriteLine("Do you want continue(y/n)");
                input = Console.ReadLine();
            }
        }
        public abstract class Shape
        {
            public abstract void Area();
        }
        public class Circle : Shape
        {
            public override void Area()
            {
                int radius;
                Console.WriteLine("Enter the radius");
                radius = Convert.ToInt32(Console.ReadLine());
                int area = (22 / 7) * radius * radius;
                Console.WriteLine("\nArea of circle:" + area);
            }
        }
            public class Rectangle : Shape
            {
                public override void Area()
                {
                    int length, breadth;
                    Console.WriteLine("Enter the length");
                    length = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter the breadth");
                    breadth = Convert.ToInt32(Console.ReadLine());
                    int area = breadth * length;
                    Console.WriteLine("\n Area of rectangle:"+ area);
                }
            }
        
    }
}

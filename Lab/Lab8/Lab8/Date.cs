﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Date : IPrintable
    {
        public int mDay;
        public int mMonth;
        public int mYear;
        public Date()
        {
            mDay = 1;
            mMonth = 1;
            mYear = 1987;
        }
        public Date(int dd, int mm, int yyyy)
        {
            mDay = dd;
            mMonth = mm;
            mYear = yyyy;
        }
        public void Print()
        {
            Console.WriteLine("Date:" + mDay + "-" + mMonth + "-" + mYear);
        }
        

    }
}

//output

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    public class Employee : IPrintable
    {
        public int count1;
        string input = "y";
        int Emp_Id;
        public string[] Emp_Name = new string[10];
        public string[] Dep_name = new string[10];
        public string[] Designation = new string[10];
        public double[] Basic_salary = new double[10];
        public double[] HRA = new double[10];
        public double[] DA = new double[10];
        public double[] provident_fund = new double[10];
        public double[] gross_salary = new double[10];
        public double[] net_salary = new double[10];
        public double Medical = 1240;
        public double PT = 1800;
        public Employee()
        {
            Emp_Id = count1;
            Dep_name[count1] = "Arts";
            Emp_Name[count1] = "unknown";
            Designation[count1] = "unknown";
            Basic_salary[count1] = 99999;
        }
        public void Store_emp_Details()
        {

            while (input == "y")
            {

                Console.WriteLine("Enter the Employee Name");
                Emp_Name[count1] = Console.ReadLine();
                Console.WriteLine("Enetr the Department Name:");
                Dep_name[count1] = Console.ReadLine();
                Console.WriteLine("Enter the Designation.:1.Employee\t2.manager\t3.president");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        Designation[count1] = "Employee";
                        Basic_salary[count1] = 15000;
                        break;
                    case 2:
                        Designation[count1] = "manager";
                        Basic_salary[count1] = 30000;
                        break;
                    case 3:
                        Designation[count1] = "president";
                        Basic_salary[count1] = 45000;
                        break;
                    default:
                        break;
                }
                Console.WriteLine("Do you want add employee again(y/n)");
                HRA[count1] = (Basic_salary[count1] * 8) / 100;
                provident_fund[count1] = (Basic_salary[count1] * 12) / 100;
                gross_salary[count1] = Basic_salary[count1] + HRA[count1] + Medical;
                net_salary[count1] = gross_salary[count1] - (PT - gross_salary[count1]);
                count1++;
                input = Console.ReadLine();
            }

        }
        public void Print()
        {
            for (int i = 0; i < count1; i++)
            {
                Console.WriteLine("\nEmployee ID:" + (i + 1) + "\nEmployee name:" + Emp_Name[i] + "\nDepartment Name:" + Dep_name[i] + "\nDesignation:" + Designation[i]);
                Console.WriteLine("HRA:" + HRA[i] + "\nPF:" + provident_fund[i] + "\nGS:" + gross_salary[i]);
                Console.WriteLine("Net Salary" + net_salary[i]);
            }

        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            IPrintable prn;
            Shape sh = new Shape();
            Circle cir = new Circle();
            Employee emp = new Employee();
            Date date = new Date(12,10,1996);
            Rectangle rectangle = new Rectangle();
            string input = "y";
            while (input == "y")
            {
                Console.WriteLine("Print Data about :\t1.Employee\t2.Date\t3.Shape");
                int ch = Convert.ToInt32(Console.ReadLine());
                switch (ch)
                {
                    case 1:
                        emp.Store_emp_Details();
                        prn = emp;
                        prn.Print();
                        break;
                    case 2:
                        date.Print();
                        break;
                    case 3:
                        cir.Print();
                        rectangle.Print();
                        Console.ReadLine();
                        break;
                    default:
                        break;

                }
                Console.WriteLine("Do you want continue(y/n)");
                input = Console.ReadLine();
            
        }
    }
    }
    public interface IPrintable
    {
        void Print();
    }
}

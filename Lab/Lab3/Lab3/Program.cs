﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp = new Employee("Designation",15000);
            Console.WriteLine("Salary Details of Employee are");
            emp.cal_salary("Employee",15000);
            Console.WriteLine("Salary Details of Manager are");
            emp.cal_salary("Manager",30000);
            Console.WriteLine("Salary Details of President are");
            emp.cal_salary("President",45000);
            Console.ReadLine();

        }
    }
}

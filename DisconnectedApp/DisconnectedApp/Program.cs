﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DisconnectedApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=TrainingDB;Integrated Security=True;Pooling=False"))
            {
                SqlDataAdapter adapter = new SqlDataAdapter("select * from Products", conn);


                string str =string.Format("insert into products(Name,Description,Category")
                DataSet ds = new DataSet();

                adapter.Fill(ds, "Products"); //connection will be opened and closed

                DataRow row = ds.Tables[0].NewRow();
                row["Name"] = "Tennis Ball";
                row["Description"] = "For child";
                row["Category"] = "tennis";
                row["Price"] = 312;

                ds.Tables[0].Rows.Add(row);
                adapter.Update(ds, "Products");


                Console.WriteLine("Columns {0} in Products");

                Console.WriteLine("Name: {0}\t Description:{1}\tCategory{2}\tPrice {3}", ds.Tables[0].Rows[0]["Name"], ds.Tables[0].Rows[0]["Description"], ds.Tables[0].Rows[0]["Category"], ds.Tables[0].Rows[0]["Price"]);

                foreach (var item in ds.Tables[0].Columns)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();
            }
        }
    }
}
